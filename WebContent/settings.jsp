<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${editUser.name}の設定</title>
<link rel="stylesheet" href="./css/style.css">
        <link rel="stylesheet" href="./css/setting.css">
</head>
<body>

	<div class="header">
		<h1>ユーザー編集</h1>
	</div>

	<div class="main">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						※<c:out value="${message}" /><br/>
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="settings" method="post">
			<br /> <input name="id" value="${editUser.id}" id="id" type="hidden" />
			<div class="name">
			<label for="name">名前</label>
			　(10文字以下)
			</div>
			<div class="name-form">
			<input name="name" value="${editUser.name}" id="name" /><br />
			</div>

			<div class="loginid">
			<label for="loginid">ログインID</label>
			　(6文字以上20文字以下)
			</div>
			<!-- <div class="loginid-form"> -->
			<input name="loginid" value="${editUser.loginId}" class="loginid-form"/><br />
			<!-- </div> -->

			<div class="password">
			<label for="password">パスワード</label>
			　(6文字以上20文字以下)
			</div>
			<div class="password-form">
			<input name="password" type="password" id="password" /><br />
			</div>

			<div class="macth">
			<label for="matchpassword">確認用パスワード</label>
			</div>
			<div class="macth-form">
			<input name="matchpassword" type="password" id="matchpassword"><br />
			</div>

			<!-- もし編集するユーザIDとlogin中のユーザーIDが違ったら -->

			<c:if test="${loginUser.id != editUser.id }">
				<div class="branchid">
				<label for="branchid">支店</label>
				</div>
				<select name="branchid">
					<c:forEach items="${branch}" var="branch">
						<c:if test="${editUser.branchId == branch.id}">
							<option value="${branch.id}" selected>${branch.name }</option>
						</c:if>

						<c:if test="${editUser.branchId != branch.id}">
							<option value="${branch.id}">${branch.name }</option>
						</c:if>

					</c:forEach>
				</select>
				<br />
				<div class="positionid">
				<label for="positionid">部署・役職</label>
				</div>
				<select name="positionid">
					<c:forEach items="${Position}" var="position">
						<c:if test="${editUser.positionId == position.id}">
							<option value="${position.id}" selected>${position.name }</option>
						</c:if>

						<c:if test="${editUser.positionId != position.id}">
							<option value="${position.id}">${position.name }</option>
							</c:if>
					</c:forEach>
				</select>
				<br />
			</c:if>

			<!-- もし編集するユーザIDとlogin中のユーザーIDが一緒なら -->
			<c:if test="${loginUser.id == editUser.id }">
				<label for="branchid">支店</label>
				<input type="hidden" name="branchid" value="${editUser.branchId}" />
	                : ${editUser.branchId}<br />

				<label for="positionid">部署・役職</label>
				<input type="hidden" name="positionid"
					value="${editUser.positionId}" />: ${editUser.positionId}<br />
			</c:if>
			<div class="submit">
			<input type="submit" value="更新" /> <br />
			</div>
			<div class="back">
			<a href="management">戻る</a>
			</div>
		</form>
		<div class="copyright">Copyright(c)Takuma Mariko</div>
	</div>
</body>
</html>