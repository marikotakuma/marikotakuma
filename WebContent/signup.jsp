<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="./css/style.css">
        <link rel="stylesheet" href="./css/signup.css">
<title>ユーザー新規登録画面</title>
</head>
<body>

	<div class="header">
	<h1>ユーザー新規登録</h1>
	</div>
	<div class="main">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						※<c:out value="${message}" /><br/>
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="signup" method="post">

			<div class="name">
			<label for="name">名前　(10文字以下)</label><br/>
			</div>
			<div class="name-form">
			<input name="name"id="name" /> <br />
			</div>

			<div class="loginid">
			<label for="loginid">ログインID　(6文字以上20文字以下)</label><br/>
			</div>

			<div class="loginid-form">
			<input name="loginId" id="loginid" /><br />
			</div>

			<div class="password">
			<label for="password">パスワード　(6文字以上20文字以下)</label><br/>
			</div>
			<div class="password-form">
			<input name="password" type="password" id="password" /> <br />
			</div>

			<div class="match-password">
			<label for="password">確認パスワード</label><br/>
			</div>
			<div class="match-form">
			<input name="matchPassword" type="password" id="password" /> <br />
			</div>


			<div class="branchid">
			<label for="branchid">支店</label><br/>
			</div>
			<div class="branchid-form">
			<select name="branchId">
				<c:forEach items="${branch}" var="branch">
					<option value="${branch.id}">${branch.name}</option>
				</c:forEach>
			</select><br />
			</div>

			<div class="positionid">
			<label for="positionid">部署・役職</label><br/>
			</div>

			<div class="positionid-form">
			<select name="positionId">
			<option value="1">人事部</option>
			<option value="2">管理部</option>
			<option value="3">店長</option>
			<option value="4">社員</option>
			</select><br />
			</div>

			<div class="post">
			<input type="submit" value="登録" /> <br />
			</div>

			<div class="back">
			<a href="management">戻る</a>
			</div>
		</form>
		<div class="copyright">Copyright(c)Takuma Mariko</div>
	</div>
</body>
</html>