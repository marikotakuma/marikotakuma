<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="./css/style.css">
<link rel="stylesheet" href="./css/newmessage.css">
<title>新規投稿</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>

	<div class="new-post">
	<h1>新規投稿</h1>
	</div>
	<div class="main-message">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li>※<c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

	<div class="form-area">
			<form action="newmessages" method="post"><br />

			<div class="subject">
				<label for="title">件名　(30文字以下)</label><br />
			</div>
			<div class="subject-form">
				<input name="subject" type="text" id="title" /><br />
			</div>

			<div class="category">
				<label for="category">カテゴリ　(10文字以下)</label><br />
			</div>
			<div class="category-form">
				<input name="category" type="text" id="category" /><br />
			</div>

			<div class="text">
				<label for="text">本文　(1000文字以下)</label>
			</div>
			<div class="text-form">
				<textarea name="text" cols="100" rows="5" class="text-box"></textarea><br />
			</div>

			<div class="post">
				<input type="submit" value="投稿">
			</div>
			<div class="back">
				<a href="./">戻る</a>
			</div>

			</form>
		</div>
		</div>
		<div class="copyright">Copyright(c)Takuma Mariko</div>

</body>
</html>