<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/style.css">
        <link rel="stylesheet" href="./css/management.css">
        <title>ユーザー管理</title>
    </head>
    <body>

            <div class="header">
            	<div class="header-name">
            	<h1>ユーザー管理</h1>
            	</div>

            <div class="menu">
            	<a href="./">TOP</a>
                <a href="signup">新規登録</a>
                <a href="logout" onClick="return logoutCheck();">ログアウト</a>
              </div>
            </div>
           <div class="main-management">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							※<c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
			</c:if>

			<!-- テーブルを作って表示させる値をループさせる -->

            	<table border="1" class="table">
            		<tr>
            			<th>名前</th>
            			<th>ログインID</th>
            			<th>支店</th>
            			<th>役職・部署</th>
            			<th>停止・復活</th>
            			<th>編集</th>

            		</tr>
            		<c:forEach items="${managements}" var="userdata">
		            	<tr ${usredata.is_deleted == 1 ? '#FF9966' : '#F5F5DC'}>
		            		<td><c:out value="${userdata.name}" /></td>
		            		<td><c:out value="${userdata.loginId}" /></td>
		            		<td><c:out value="${userdata.branch_name}" /></td>
		            		<td><c:out value="${userdata.postion_name}" /></td>

							<!-- ログイン中のユーザーのみ表示させるメッセージ -->
							<td>
								<c:if test="${loginUser.id == userdata.id}">
								<a>ログイン中</a>
								</c:if>


								<!-- ボタンを表示させるための条件 -->

								<c:if test="${loginUser.id != userdata.id}">

			     	      		<!-- 停止ボタン -->

				     	      	<c:if test="${userdata.is_deleted == 0}">
				     	      		<form action="management" method="post">
				     	      			<input type="hidden" name="userid" value="${userdata.id}">
				     	      			<input type="hidden" name="isdeleted" value="0">
				     	      			<input type="submit" value="停止" onClick="return stopCheck();">
				     	      		</form>
				     	      	</c:if>


				     	      	<!-- 復活ボタン -->

				     	      	<c:if test="${userdata.is_deleted == 1 }">
				     	      		<form action="management" method="post">
				     	      		<input type="hidden" name="userid" value="${userdata.id}">
				     	      			<input type="hidden" name="isdeleted" value="1">
				     	      			<input type="submit" value="復活" onClick="return submitCheck();">
				     	      		</form>
				     	      	</c:if>
				     	      	</c:if>
			     	      	</td>
		            		<td><a href="settings?editId=${userdata.id}" >編集</a></td>
		            	</tr>
	            	</c:forEach>
	            </table>
	            </div>
            <div class="copyrights"> Copyright(c)Takuma Mariko</div>

        <!-- 確認ダイアログ -->
	<script type="text/javascript">
		function stopCheck() {

			//表示させるダイアログを変数に入れる
			var result = window.confirm('停止させますか？');

			if (result) {

				//return trueで返す
				return true;
			}

			return false;
		}
	</script>
	<script type="text/javascript">
		function submitCheck() {

			//表示させるダイアログを変数に入れる
			var result = window.confirm('復活させますか？');

			if (result) {

				//return trueで返す
				return true;
			}

			return false;
		}
	</script>
		<script type="text/javascript">
		function logoutCheck() {

			//表示させるダイアログを変数に入れる
			var result = window.confirm('ログアウトしますか?');

			if (result) {

				//return trueで返す
				return true;
			}

			return false;
		}
		</script>
</body>
</html>
