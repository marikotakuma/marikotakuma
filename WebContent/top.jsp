<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="./css/style.css">
<link rel="stylesheet" href="./css/top.css">
<link rel="stylesheet"
	href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/trontastic/jquery-ui.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<script>
	$(function() {
		$("#datepicker").datepicker();
		$("#datepicker2").datepicker();
	});
</script>
<title>ホーム</title>
</head>
<body>
	<div class="header">
		<div class="headline">
			<h1>TOP</h1>
		</div>

		<div class="menu">
		<!-- 権限のあるユーザーのみ表示させるため -->
		<c:if test="${loginUser.positionId == 1}">
			<a href="management">ユーザー管理</a>
		</c:if>
		<a href="newmessages">新規投稿</a> <a href="logout" onClick="return logoutCheck();">ログアウト</a>
		</div>

		<!-- 検索機能 -->
		<div class="search">
			<form action="index.jsp" method="get">
				<label for="category">カテゴリー</label>
				<input name="category" value="${search}" />

				<label for="createdate">日付</label>
				<input name="datesearch" value="${datesearch}" id="datepicker" />
				<a>～</a>

				<input name="postsearch" value="${postsearch}" id="datepicker2" />

				<input type="submit" value="検索">
			</form>
		</div>
	</div>

	<div class="main-contents">
		<!-- エラーメッセージ表示 -->
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						※<c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<!-- 投稿表示画面 -->
		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="post">

					<div class="account-name">
						<span class="name"></span>
					</div>


					<div class="subject">
						件名：
						<c:out value="${message.subject}"></c:out>
					</div>

					<div class="category">
						カテゴリー：
						<c:out value="${message.category}"></c:out>
					</div>




					<!-- 投稿文の改行処理 -->
					<div class="text">
						<c:forEach var="text" items="${fn:split(message.text,'
					') }">
							<c:out value="${text}" />
							<br>
						</c:forEach>
					</div>

					<div class="name">
							<div class="post-name">
							投稿者：
							<c:out value="${message.name}" />
							</div>
							<div class="date">
							<fmt:formatDate value="${message.created_date}"
								pattern="yyyy/MM/dd HH:mm:ss" />
						</div>

						<!-- 削除ボタン -->

						<div class="delete">
							<c:if test="${message.userId == loginUser.id }">
								<form action="index.jsp" method="post">
									<input type="hidden" name="delete" value="0"> <input
										type="hidden" name="id" value="${message.id}"> <input
										type="submit" value="削除">
								</form>
							</c:if>
						</div>
					</div>
					<div class="none"></div>
				</div>

				<!-- コメント表示画面 -->

				<c:forEach items="${comments}" var="comment">
					<c:if test="${message.id == comment.messageId }">
						<div class="comment">
						<div class="aaa"></div>
							<div class="comment-name"><c:out value="${comment.name}"></c:out></div>

							<div class="comment-text">
								<c:forEach var="text"
									items="${fn:split(comment.text,'
						') }">
									<c:out value="${text}" />
									<br>
								</c:forEach>
							</div>

							<div class="comment-date">
								<fmt:formatDate value="${comment.created_date}"
									pattern="yyyy/MM/dd HH:mm:ss" />
							</div>

							<div class="post-delete">
								<c:if test="${comment.userId == loginUser.id }">
									<form action="./" method="post">
										<!-- postで同じ処理をするのでhiddenで数字を持たせてる -->
										<input type="hidden" name="deleteflag" value="0"> <input
											type="hidden" name="id" value="${comment.id}"> <input
											type="submit" value="削除">
									</form>
								</c:if>
							</div>
							<div class="comment-none"></div>
						</div>
					</c:if>
				</c:forEach>



				<!-- コメントエリア部分 -->
				<div class="comment-form">
					<form action="index.jsp" method="post">
						<!-- どの投稿に対してのコメントかをわからせるためにidを取得している -->
						<input name="id" value="${message.id}" id="id" type="hidden" />
						<label for="textarea" class="lable">コメント</label>　(500文字以内)<br />
						<textarea name="comment" cols="100" rows="5" class="text-box"></textarea><br />
						<div class="post">
						<input type="submit" value="投稿">
						</div>
					</form>
				</div>
			</c:forEach>
		</div>
	</div>
	<div class="copyright">Copyright(c)Takuma Mariko</div>

	<!-- 確認ダイアログ -->
	<script type="text/javascript">
		function logoutCheck() {

			//表示させるダイアログを変数に入れる
			var result = window.confirm('ログアウトしますか?');

			if (result) {

				//return trueで返す
				return true;
			}

			return false;
		}
	</script>
</body>
</html>
