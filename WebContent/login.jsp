<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="./css/style.css">
<link rel="stylesheet" href="./css/login.css">
<title>ログイン</title>
</head>
<body>

	<div class="top">
		<h1>管理システム</h1>
	</div>

	<div class="main-area">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							※<c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>


		<div class="login-form">
			<form action="login" method="post">
				<br /> <label for="loginId">ログインID</label>
				<input name="loginId" value="${loginId}"  class="login-id"/> <br />
				<label for="password">パスワード</label>
					<input name="password" type="password" id="password" class="password"/> <br />
					<div class="login">
					<input type="submit" value="ログイン"  /> <br />
					</div>
			</form>
		</div>
	</div>
	<div class="copyright">Copyright(c)Takuma Mariko</div>
</body>
</html>