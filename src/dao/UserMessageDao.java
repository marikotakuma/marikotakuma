package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> getUserMessages(Connection connection, int num, String search,
			String datesearch, String postsearch) {

		//投稿表示のための表結合SQLと検索するための条件SQL

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("messages.subject as subject, ");
			sql.append("messages.text as text, ");
			sql.append("messages.category as category, ");
			sql.append("users.name as name,");
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("WHERE messages.created_date BETWEEN  ? ");
			sql.append("AND (? + INTERVAL 23 HOUR + INTERVAL 59 MINUTE + INTERVAL 59 SECOND)" );

			//カテゴリの中が空じゃなかったら処理

			sql.append("AND category LIKE ? ");


			sql.append("ORDER BY created_date DESC limit " + num);


			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, datesearch);
			ps.setString(2, postsearch);
			ps.setString(3, "%" + search + "%");


			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageList(ResultSet rs)
			throws SQLException {

		//
		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String subject =rs.getString("subject");
				String text = rs.getString("text");
				String category =rs.getString("category");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserMessage message = new UserMessage();
				message.setName(name);
				message.setId(id);
				message.setUserId(userId);
				message.setSubject(subject);
				message.setText(text);
				message.setCategory(category);
				message.setCreated_date(createdDate);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}