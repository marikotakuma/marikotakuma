package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import beans.Position;
import exception.SQLRuntimeException;

public class PulldownDao {


	//positinを表示させるためのSQL
	public List<Position> getPosition(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM postions";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			//positionのbeansにlistを使って入れている
			List<Position> positionList = toPositionList(rs);
			return positionList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<Branch> toBranchList(ResultSet rs) throws SQLException {

		List<Branch> ret = new ArrayList<Branch>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");

				Branch user = new Branch();
				user.setId(id);
				user.setName(name);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}public List<Branch> getBranch(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM branches ";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			//positionのbeansにlistを使って入れている
			List<Branch> branchList = toBranchList(rs);

			return branchList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<Position> toPositionList(ResultSet rs) throws SQLException {

		List<Position> position = new ArrayList<Position>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");

				Position user = new Position();
				user.setId(id);
				user.setName(name);

				position.add(user);
			}
			return position;
		} finally {
			close(rs);
		}
	}
}
