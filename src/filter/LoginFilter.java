/**
 *
 */
package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(filterName="LoginFilter", urlPatterns="/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

		//URLPathを取得している
		String url = ((HttpServletRequest)req).getServletPath();

		//取得したURLがログインだったら
		if (url.equals("/login") || url.substring(url.length() - 3).equals("css")) {
			chain.doFilter(req, res);
			return;
		}

		// セッションが存在しない場合NULLを返す
		HttpSession session = ((HttpServletRequest)req).getSession();

		if(session.getAttribute("loginUser") != null) {
		    // セッションがNULLでなければ、通常どおりの遷移
		    chain.doFilter(req, res);
		    return;
		}

		List<String> messages = new ArrayList<String>();
		messages.add("ログインしていません");
		session.setAttribute("errorMessages", messages);
	    // セッションがNullならば、ログイン画面へ飛ばす

	    ((HttpServletResponse) res).sendRedirect("login");
	}

	@Override
	public void init(FilterConfig paramFilterConfig) throws ServletException {
		// TODO 自動生成されたメソッド・スタブ
	}

	@Override
	public void destroy() {
		// TODO 自動生成されたメソッド・スタブ
	}
}
