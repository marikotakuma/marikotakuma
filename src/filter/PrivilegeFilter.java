package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(filterName="AuthorityFilter", urlPatterns = {"/management","/signup","/setting"} )
public class PrivilegeFilter implements Filter {

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

		//セッションを取得
		HttpSession session = ((HttpServletRequest)req).getSession();

		//セッションからloginUserを取得して
		User user = (User) session.getAttribute("loginUser");

		if(user.getPositionId().equals("1")) {

		    chain.doFilter(req, res);

		    return;
		}

		List<String> messages = new ArrayList<String>();
		messages.add("権限なし");
		session.setAttribute("errorMessages", messages);
	    // 権限がないならホーム画面に飛ばす
	    ((HttpServletResponse) res).sendRedirect("index.jsp");
	}

	@Override
	public void init(FilterConfig paramFilterConfig) throws ServletException {
		// TODO 自動生成されたメソッド・スタブ
	}

	@Override
	public void destroy() {
		// TODO 自動生成されたメソッド・スタブ
	}
}
