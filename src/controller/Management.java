package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;


@WebServlet(urlPatterns = { "/management" })
public class Management extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<User> managements = new UserService().getManagement();

		request.setAttribute("managements", managements);


		request.getRequestDispatcher("/management.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		String isdeleted = request.getParameter("isdeleted");
		int userid =Integer.parseInt(request.getParameter("userid"));

		if(isdeleted != null && isdeleted.equals("0")) {
			int id =Integer.parseInt(request.getParameter("isdeleted"));

			new UserService().isdeleted(id,userid);

		}else if(isdeleted != null && isdeleted.equals("1")) {
			int id =Integer.parseInt(request.getParameter("isdeleted"));
			new UserService().isdeleted(id, userid);
		}
		response.sendRedirect("management");
	}
}
