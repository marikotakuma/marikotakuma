package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.PulldownService;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ログインユーザー情報のidを元にDBからユーザー情報取得
		User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("editId")));

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();

		// プルダウンを表示させるためにBeansから値を取得しリストを作っている
		List<Branch> branch = new PulldownService().getBranch();
		request.setAttribute("branch", branch);

		List<Position> position = new PulldownService().getPosition();
		request.setAttribute("Position", position);

		// Beansのなかにデータがなかったら
		if (editUser == null) {
			messages.add("不正なパラメーターです");

			session.setAttribute("errorMessages", messages);

			response.sendRedirect("management");
		} else {

			request.setAttribute("editUser", editUser);

			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();

		User editUser = getEditUser(request);

		if (isValid(request, messages) == true) {

			try {

				new UserService().update(editUser);

			} catch (NoRowsUpdatedRuntimeException e) {

				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");

				session.setAttribute("errorMessages", messages);

				request.setAttribute("editUser", editUser);

				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}

			response.sendRedirect("management");
		} else {
			session.setAttribute("errorMessages", messages);

			request.setAttribute("editUser", editUser);

			request.getRequestDispatcher("settings.jsp").forward(request, response);

		}
	}

	private User getEditUser(HttpServletRequest request) throws IOException, ServletException {

		User editUser = new User();

		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setLoginId(request.getParameter("loginid"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setBranchId(request.getParameter("branchid"));
		editUser.setPositionId(request.getParameter("positionid"));
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String loginId = request.getParameter("loginid");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String branchId = request.getParameter("branchid");
		String positionId = request.getParameter("positionid");
		String matchPassword = request.getParameter("matchpassword");

		if (StringUtils.isEmpty(name)) {
			messages.add("名前を入力してください");
		}else{if (name.length() >= 10) {
			messages.add("名前は10文字以下で入力してください");
		}
		}

		if (StringUtils.isEmpty(loginId)) {
			messages.add("ログインIDを入力してください");
		}else{
		if (!(loginId.length() >= 6 && loginId.length() <= 20)) {
			messages.add("ログインIDは６文字以上２０文字以下で入力してください");
		}
		}

		if (!(StringUtils.isBlank(password))) {

			if (!(password.length() >= 6 && password.length() <= 20)) {
				messages.add("パスワードは６文字以上２０文字以下で入力してください");
			}

			if (!(password.equals(matchPassword))) {
				messages.add("パスワードが一致しません");
			}

		}



		if (StringUtils.isEmpty(branchId)) {
			messages.add("支店を入力してください");
		}

		if (StringUtils.isEmpty(positionId)) {
			messages.add("部署・役職を入力してください");
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
