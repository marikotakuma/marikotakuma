package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.PulldownService;
import service.UserService;


@WebServlet(urlPatterns = {"/signup"})
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<Branch> branch = new PulldownService().getBranch();
		request.setAttribute("branch", branch);

		List<Position> position = new PulldownService().getPosition();
		request.setAttribute("Position", position);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		if(isValid(request, messages) == true) {

			User user = new User();
			user.setLoginId(request.getParameter("loginId"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranchId(request.getParameter("branchId"));
			user.setPositionId(request.getParameter("positionId"));

			new UserService().register(user);

			response.sendRedirect("management");
		}else{
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("signup");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String branchId = request.getParameter("branchId");
		String positionId = request.getParameter("positionId");
		String matchPassword = request.getParameter("matchPassword");

		if (StringUtils.isEmpty(name)) {
			messages.add("名前を入力してください");
		}else{
		if (name.length() >= 10) {

		messages.add("名前は10文字以下で入力してください");
		}
		}

		if (StringUtils.isEmpty(loginId)) {
			messages.add("ログインIDを入力してください");
		}else{
		if(! (loginId.length() >= 6 && loginId.length() <= 20)) {
			messages.add("ログインIDは６文字以上２０文字以下で入力してください");
		}
		if (! (loginId.matches("^[0-9a-zA-Z]+$"))) {
			messages.add("ログインIDは半角英数字で入力してください");
		}
		}
		if (new UserService().getloginId(loginId) == false) {
			messages.add("ログインIDが重複しています");
		}

		if (StringUtils.isEmpty(password)) {
			messages.add("パスワードを入力してください");
		}else{ if(! (password.matches("^[ -~]+$"))) {
			messages.add("パスワードは記号を含む半角英数字で入力してください");
		}
		}
		if (!(password.equals(matchPassword))) {
			messages.add("パスワードが一致しません");
		}




		if (StringUtils.isEmpty(branchId)) {
			messages.add("支店を入力してください");
		}

		if (StringUtils.isEmpty(positionId)) {
			messages.add("部署・役職を入力してください");
		}


		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
