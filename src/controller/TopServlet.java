
package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {


		//検索機能
		String search =(request.getParameter("search"));
		String datesearch =(request.getParameter("datesearch"));
		String postsearch =(request.getParameter("postsearch"));
		search = "";
		datesearch = "2018/01/01 00:00";
		postsearch = "2020/3/31 23:59";

		if(!StringUtils.isBlank(request.getParameter("category"))) {
			search = request.getParameter("category");
			request.setAttribute("search", search);
		}
		if(!StringUtils.isBlank(request.getParameter("datesearch"))) {
			datesearch = request.getParameter("datesearch");
			System.out.println(datesearch);
			request.setAttribute("datesearch", datesearch);
		}
		if(!StringUtils.isBlank(request.getParameter("postsearch"))) {
			postsearch = request.getParameter("postsearch");
			request.setAttribute("postsearch", postsearch);
		}

		//getMessageにJSPから取得した値を引数として渡している
		List<UserMessage> messages = new MessageService().getMessage(search,datesearch,postsearch);
		List<Comment> comments = new CommentService().getComment();

		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);



		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> message = new ArrayList<String>();

		String deleteflag =(request.getParameter("deleteflag"));
		String delete =(request.getParameter("delete"));

		//コメント削除の処理
		if(deleteflag != null && deleteflag.equals("0")) {
			int id =Integer.parseInt(request.getParameter("id"));
			new CommentService().delete(id);

		//投稿削除の処理
		} else if(delete != null && delete.equals("0")) {
			int id =Integer.parseInt(request.getParameter("id"));
			new MessageService().delete(id);

		//コメント入力機能の処理
		} else if (isValid(request, message) == true) {

			User user = (User) session.getAttribute("loginUser");

			Comment comments = new Comment();
			comments.setText(request.getParameter("comment"));
			comments.setUserId(user.getId());
			comments.setMessageId(Integer.parseInt(request.getParameter("id")));

			new CommentService().register(comments);

		} else {
			session.setAttribute("errorMessages", message);
		}
		response.sendRedirect("./");

	}

	//コメント入力のエラー処理
	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String message = request.getParameter("comment");

		if (StringUtils.isBlank(message) == true) {
			messages.add("コメントを入力してください");
		}
		if (500 < message.length()) {
			messages.add("500文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}

