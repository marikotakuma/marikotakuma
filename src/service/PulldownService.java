package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Branch;
import beans.Position;
import dao.PulldownDao;

public class PulldownService {

	public List<Position> getPosition() {

		Connection connection = null;
		try {
			connection = getConnection();

			PulldownDao pulldownDao = new PulldownDao();
			List<Position> pulldown = pulldownDao.getPosition(connection);

			commit(connection);

			return pulldown;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public List<Branch> getBranch() {

		Connection connection = null;
		try {
			connection = getConnection();

			PulldownDao pulldownDao = new PulldownDao();
			List<Branch> pulldown = pulldownDao.getBranch(connection);

			commit(connection);

			return pulldown;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}